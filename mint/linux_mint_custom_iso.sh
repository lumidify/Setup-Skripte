#!/bin/bash
#you need to download the latest Linux Mint ISO from the website first before running the script.

#catch if argument 1 is empty. Without the ISO as an argument this script won't work.
if [[ -z $1 ]]; then 
  echo "Usage: $0 [linuxmint-original.iso]"
  exit
#catch if the file cannot be found or is misspelled.
elif [[ ! -f $1 ]]; then
  echo "File not found: $1" >&2
  exit 1
fi

UNATTENDED="unattended-$1" # => unattended-linuxmint-19.3-cinnamon-64bit.iso

#creates a folders that is used during the process.
mkdir iso

#copies the iso to the created folder and makes the installed files writable.
bsdtar -xf "$1" -C ./iso
chmod -R u+w ./iso

#copies the computertruhe.seed from codeberg-repo so always the latest preseed-file is used.
curl https://codeberg.org/Computertruhe/Setup-Skripte/raw/branch/master/mint/computertruhe.seed > ./iso/preseed/linuxmint.seed

#creates the custom-iso with the given parameters.
mkisofs -D -r -V "UNATTENDED_MINT" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o "$UNATTENDED" ./iso

#umounts the iso and clears up the space we used to create the iso.
rm -rf ./iso

#from here we are basically done and the iso can be "burned" on an USB ready for install.
echo "Die Custom ISO wurde fertiggestellt und kann nun verwendet werden. Die ISO ist unter $UNATTENDED zu finden."

