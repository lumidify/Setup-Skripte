
Anleitung zur Vorbereitung einer Windows10-ISO für eine (fast) automatische Installation

benötigte Software:

+ Windows 10 ISO in neuester Version
+ Rufus: [https://rufus.ie/](https://rufus.ie/)
+ ImgBurn: [http://www.imgburn.com/](http://www.imgburn.com/)
+ autounattend.xml [hier aus dem repo runterladen](../windows/autounattend.xml)
+ Powershell-Script für die Installation [hier aus dem repo runterladen](../windows/Setup.ps1)
+ USB-Stick mind. 8GB 
+ Einen Rechner auf dem Windows installiert ist

Nachdem alle notwendige Software installiert ist können wir beginnen. 

a)  Entpacke die Windows 10-ISO an einen Ort wo du sie wiederfindest 

1. die Windows 10 kann unter Windows sehr einfach als Laufwerk gemountet werden. Entweder indem man einen Doppelklick auf die ISO macht oder einen Rechtsklick auf die ISO macht und dann „Bereitstellen“ auswählt. ![ISO bereitstellen durch rechtsklick](images/02.png "Windows-ISO bereitstellen") 

Die Windows-ISO wird damit als neues Laufwerk gemountet und in einem Fenster geöffnet. (In meinem Fall ist der Laufwerkbuchstabe H:) 

![ISO ist bereit gestellt](images/03.png "bereitgestellte ISO") 

2. Kopiere den gesamten Inhalt der Windows-ISO. Mit Strg+A kann der gesamte Inhalt markiert werden. Dann Rechtsklick → Kopieren auswählen 

![alle Inhalte der ISO mit STRG+A markieren](images/04.png "Inhalte der Windows ISO markieren") 

![Inhalte der ISO kopieren](images/05.png "Inhalte der Windows ISO kopieren") 

3. Füge das kopierte an der vorher gewählten Stelle wieder ein. In meinem Fall ist der Ordner "Win 10" 

![Kopierte Datein an gewählter Stelle wieder einfügen](images/06.png "kopierte Datein wieder einfügen") 

b) Kopiere die vorher runtergeladene autounattend.xml in das Stammverzeichnis der entpackten ISO, sodass es aussieht wie hier:

![Stammverzeichnis der ISO nach kopieren der autounattend.xml](images/07.png "neues Stammverzeichnis der ISO") 

c) Als nächstes muss noch das Powershell-Script an die passende der ISO kopieren, dies ist allerdings etwas aufwändiger und braucht eine Powershell. Bitte der Anleitung an dieser Stelle Schritt für Schritt folgen und die Pfade mit den korrekten Pfaden ersetzen:

1. Öffne eine Administrator-Powershell. Dazu kann einfach mit derWindows-Suche im Startmenü nach Powershell gesucht werden. Mit einem Rechtsklick kann eine Administrator-Powershell geöffnet werden:

![öffne Administrator-Powershell](images/08.png "öffne Administrator-Powershell")

2. Prüfe nochmals dass du in einer Administrator-Powershell bist. Wenn alles korrekt ist, sollte „system32“ im Pfad zu sehen sein:

![Prüfe das im Pfad auch system32 vorhanden ist](images/09.png "Pfad mit system32")
	
3. Wechsle nun in das Hauptverzeichnis der Systemplatte (in meinem Fall ist dies C:). Dazu kannst du den „cd“-Befehl nutzen. Für meinen Fall lautet das Command also:

        cd C:\

4. Erstelle dort ein Verzeichnis. Das wirst du später brauchen. Ich hab meines „iso“ genannt. Um das Verzeichnis zu erstellen lautet der Befehl:

        mkdir iso

5. Nutze jetzt das Tool DISM in der Powershell um die install.wim der ausgepackten ISO zu inspizieren. Dazu kommt folgender Befehl zum Einsatz (der Pfad ist entsprechend anzuspassen): 

        dism /Get-WimInfo /WimFile:C:\Users\Stefan\Desktop\test\Win10\sources\install.wim

Dies zeigt alle in der ISO vorhandenen Installationsmöglichkeiten, wie hier zu sehen: 

![alle Installationsmöglichkeiten einer Windows-ISO](images/10.png "Installationsmöglichkeiten der Windows-ISO")

6. Mounte den entsprechenden Index (Nummer der Installationsmöglichkeit aus der Liste zuvor) in in Nr. 4 erstellten Ordner. In meinem Beispiel habe ich Windows 10 Home gewählt, deshalb ist meine Index-Nummer 1. Für Windows 10 Pro müsste Index-Nummer 5 gewählt werden. Gemountet wird die install.wim mit folgendem Befehl:

        dism /Mount-Wim /WimFile:C:\Users\Stefan\Desktop\test\Win10\sources\install.wim /index:1 /MountDir:C:\iso

wenn der Vorgang abgeschlossen ist befindet sich unter C:\iso die Systemfestplatte die bei einer neuen Windows-Installation kopiert wird. Dies bedeutet wenn wir dort eine Datei hineintun und die install.wim wieder schließen. Bei einer neuen Installation mit dem modifizierten ISO ist die Datei dann in der neuen Installation auch an der gleichen Stelle zu finden. 

7. Kopiere nun die am Anfang runtergeladene Setup.ps1 (Powershell-Script) in das Verzeichnis C:\iso sodass es aussieht wie hier (für das Kopieren ist eine Bestätigung als Administrator erforderlich.):

![Setup.ps1 Powershell-script im iso ordner](images/11.png "Setup.ps1 in der ISO")

8. Sobald dies erledigt ist musst du nur noch die Änderungen „commiten“ und die install.wim unmounten. Stelle vorher sicher dass kein Fenster des Windows-Explorer mehr geöffnet ist! Dies erledigt dieser Befehl:

        dism /Unmount-Image /MountDir:C:\iso /Commit

9. (optinal) Die Schritte 5 – 8 müssen nun noch für alle anderen Indexe wiederholt werden, die das Script ebenfalls enthalten sollen. Statt Index 1 für Windows 10 Home kann dann bsw. Index-Nummer 5 für Windows 10 Pro verwendet werden. Wenn alle gewünschten Indexe modifiziert sind kann mit der Anleitung fortgefahren werden.

d) Im nächsten Schritt erstellen wir aus unserem Ordner mit der entpackten ISO wieder eine Windows 10-ISO die auf einen USB-Stick kopiert werden kann und für eine Installation verwendet werden kann. 

Dazu startest du einfach das Programm ImgBurn und folgst den Screenshots:

![ImgBurn: ISO aus Ordner erstellen](images/12.png "ImgBurn: ISO aus Ordner erstellen")
	
![ImgBurn: Auswahl "Erweitert" im geöffneten Fenster](images/13.png "ImgBurn: Auswahl Erweitert")

![ImgBurn: Auswahl startfähige Disc](images/14.png "ImgBurn: Auswahl startfähige Disc")
	
![ImgBurn: Häckchen setzen bei Image startfähig machen](images/15.png "Häckchen setzen bei Image startfähig machen")
	
![ImgBurn: Bootimage aus Windows 10 extrahieren](images/16.png "Bootimage aus gemounteter Windows 10 ISO erstellen")
	
![ImgBurn: Bootimage speichern](images/17.png "Bootimage speichern")
	
![ImgBurn: Speicherort wählen und speichern klicken](images/18.png "Speicherort auswählen")
	
![ImgBurn: Meldung bestätigen: Operation successful](images/19.png "Schaltfläche bestätigen")
	
![ImgBurn: Bestätigung der Verwendung des Bootimages für das jetzige Project](images/20.png "Bestätigung Verwendung Bootimage für das Projekt")
	
![ImgBurn: Quelle durchsuchen Symbol auswählen](images/21.png "Lokalen Speicher durchsuchen")
	
![ImgBurn: Ordner mit entpackter ISO auswählen](images/22.png "Ordner mit entpackter ISO wählen")
	
![ImgBurn: Ziel durchsuchen](images/23.png "Ziel durchsuchen")
	
![ImgBurn: Zielordner wählen. Name der ISO Windows10-unattended.iso](images/24.png "Ziel und Name auswählen")
	
![ImgBurn: ISO-Erstellung starten](images/25.png "ISO-Erstellung starten")
	
![ImgBurn: Abfrage zum Medien-Namen mit JA bestätigen](images/26.png "Dialog bestätigen")
	
![ImgBurn: Zusammenfassung bestätigen](images/27.png "Zusammenfassung bestätigen")
	
![ImgBurn: Bestätigung nach Abschluss der Operation](images/28.png "Abschluss der Operation")
	
e) im letzten Schritt kann die ISO dann mithilfe des Programms Rufus auf einen USB-Stick kopiert werden. Starte dazu das Programm Rufus und folge der Anleitung:

![Rufus: Klick auf Auswahl](images/29.png "Auswahl ISO")
	
![Rufus: Auswahl der erstellten ISO](images/30.png "Auswahl ISO")

Bei der Erstellung des Sticks darauf achten ob das Zielsystem BIOS oder UEFI nutzt und die Einstellungen entsprechend anpassen. (siehe Screenshot 31-UEFI oder 31-BIOS)

BIOS:

![Rufus: Einstellungen für Boot-Stick BIOS](images/31-BIOS.png "Einstellungen BIOS-USB")

UEFI:

![Rufus: Einstellungen für Boot-Stick UEFI](images/31-UEFI.png "Einstellungen UEFI-USB")
	
![Rufus: Warnung Überschreiben USB Bestätigung](images/32.png "Betstätigung Warnung")

Sobald Rufus die ISO auf den Stick geschrieben hat kann der USB-Stick für die Installation von Windows 10 verwendet werden.
